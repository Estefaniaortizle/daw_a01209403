<?php

function connect_db()
{
    $servername = "localhost";
    $username = "root";
    $password = "";
    $db = "jurassic_park";
    $con = mysqli_connect($servername, $username, $password, $db);
    if (!$con) {
        die("Connection failed: " . mysqli_connect_error());
    }
    return $con;
}

function consultar_lugares($con)
{
    $query = (" SELECT *
                FROM lugares");
    $result = mysqli_query($con, $query);
    $response = [];
    $i = 0;
    while ($rs = mysqli_fetch_assoc($result)) {
        $idLugar = $rs['idLugar'];
        $rs['seguridad'] = consult_seguridad($idLugar, $con);
        $response[$i] = $rs;
        $i += 1;
    }
    return $response;
}

function consultar_incidentes($con)
{
    $query = (" SELECT *
                FROM tiposincidentes");
    $result = mysqli_query($con, $query);
    $response = [];
    $i = 0;
    while ($rs = mysqli_fetch_assoc($result)) {
        $response[$i] = $rs;
        $i += 1;
    }
    return $response;
}

function consult_seguridad($idLugar, $con)
{
    $query = (" SELECT * FROM seguridad as se, tiposincidentes as inc
                WHERE inc.idIncidente=se.idIncidente
                and idLugar = '$idLugar'");
    $result = mysqli_query($con, $query);
    $response = [];
    $i = 0;
    while ($rs = mysqli_fetch_assoc($result)) {
        $response[$i] = $rs;
        $i += 1;
    }
    return $response;
}

function insertar_seguridad($idLugar, $idIncidente, $con)
{
    $query = ("INSERT INTO seguridad
                VALUES ('$idLugar', '$idIncidente', NOW())");
    mysqli_query($con, $query);
}

function insertar_lugar($nombre, $con)
{
    $query = ("INSERT INTO lugares
                VALUES (0, '$nombre')");
    mysqli_query($con, $query);
}

function consultar_stats($con)
{
    $response = [];
    $i = 0;
    $query = (" SELECT COUNT(*) total
                FROM lugares");
    $result = mysqli_query($con, $query);
    $response['total'] = mysqli_fetch_assoc($result)['total'];
    $i += 1;

    $query = (" SELECT COUNT(*) electricas
                FROM seguridad
                WHERE idIncidente=1");
    $result = mysqli_query($con, $query);
    $response['electricas'] = mysqli_fetch_assoc($result)['electricas'];
    $i += 1;

    $query = (" SELECT COUNT(*) herbivoros
                FROM seguridad
                WHERE idIncidente=2");
    $result = mysqli_query($con, $query);
    $response['herbivoros'] = mysqli_fetch_assoc($result)['herbivoros'];
    $i += 1;

    $query = (" SELECT COUNT(*) velociraptors
                FROM seguridad
                WHERE idIncidente=3");
    $result = mysqli_query($con, $query);
    $response['velociraptors'] = mysqli_fetch_assoc($result)['velociraptors'];
    $i += 1;

    $query = (" SELECT COUNT(*) trex
                FROM seguridad
                WHERE idIncidente=4");
    $result = mysqli_query($con, $query);
    $response['trex'] = mysqli_fetch_assoc($result)['trex'];
    $i += 1;

    $query = (" SELECT COUNT(*) ADN
                FROM seguridad
                WHERE idIncidente=5");
    $result = mysqli_query($con, $query);
    $response['ADN'] = mysqli_fetch_assoc($result)['ADN'];
    $i += 1;

    $query = (" SELECT COUNT(*) Autos
                FROM seguridad
                WHERE idIncidente=6");
    $result = mysqli_query($con, $query);
    $response['Autos'] = mysqli_fetch_assoc($result)['Autos'];
    $i += 1;

    $query = (" SELECT COUNT(*) Zonas
                FROM seguridad
                WHERE idIncidente=6");
    $result = mysqli_query($con, $query);
    $response['Zonas'] = mysqli_fetch_assoc($result)['Zonas'];
    $i += 1;

    return $response;
}

function consultar_historial($con)
{
    $query = (" SELECT lugares.Nombre, tiposincidentes.nombre, seguridad.fecha
    from lugares, tiposincidentes, seguridad
    where lugares.idLugar = seguridad.idLugar
    and seguridad.idIncidente = tiposincidentes.idIncidente
    ORDER BY seguridad.fecha DESC");
    $result = mysqli_query($con, $query);
    $response = [];
    $i = 0;
    while ($rs = mysqli_fetch_assoc($result)) {
        $response[$i] = $rs;
        $i += 1;
    }
    return $response;
}

?>
