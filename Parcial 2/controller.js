app.controller("controller", function ($scope, $http) {
  $scope.lugar_nuevo = [
    {
      nombre: "",
    },
  ];

  $scope.consultar_lugares = function () {
    $http.get("./model_lugares_qry.php").then(function (response) {
      $scope.lugares = response.data;
      console.log($scope.lugares);
    });
  };

  $scope.consultar_stats = function () {
    $http.get("./model_stats_qry.php").then(function (response) {
      $scope.stats = response.data;
      console.log($scope.stats);
    });
  };

  $scope.consultar_historial = function () {
    $http.get("./model_historial_qry.php").then(function (response) {
      $scope.historial = response.data;
      console.log($scope.historial);
    });
  };

  function allAlertsOff(){
    $scope.alertInsertar=false;
  }
  allAlertsOff();

  $scope.closealertInsertar=function(){$scope.alertInsertar=false;}

  $scope.consultar_lugares();
  $scope.consultar_stats();
  $scope.consultar_historial();

  $scope.tab_bienv = true;
  $scope.tab_consult = false;
  $scope.tab_insert = false;
  $scope.tab_historial = false;
  $scope.tab_stats = false;
  $scope.tab_filtro = false;

  $scope.showConsultar = function() {
    $scope.tab_bienv = false;
    $scope.tab_consult = true;
    $scope.tab_insert = true;
    $scope.tab_historial = false;
    $scope.tab_stats = false;
    $scope.tab_filtro = false;
  };

  $scope.showHistorial = function () {
    $scope.tab_bienv = false;
    $scope.tab_consult = false;
    $scope.tab_insert = false;
    $scope.tab_stats = false;
    $scope.tab_historial = true;
    $scope.tab_filtro = true;
  };

  $scope.showEstadisticas = function () {
    $scope.tab_bienv = false;
    $scope.tab_consult = false;
    $scope.tab_insert = false;
    $scope.tab_stats = true;
    $scope.tab_historial = false;
    $scope.tab_filtro = false;
  };

  $scope.agregarIncidente = function (incidente_seleccionado, idLugar) {
    if (incidente_seleccionado) {
      let data = [];
      data[0] = idLugar;
      data[1] = incidente_seleccionado.idIncidente;
      let url = "./model_seguridad_insertar.php";
      $http.post(url, data).then(function (response) {
        $scope.consultar_lugares();
        $scope.consultar_stats();
        $scope.consultar_historial();
      });
    } else {
      alert("Por favor seleccione un incidente");
    }
  };

  $scope.agregarLugar = function () {
    console.log($scope.lugar_nuevo);
    var url = "model_lugar_insertar.php";
    $http.post(url, $scope.lugar_nuevo).then(function (response) {
      console.log(response.data);
      $scope.consultar_lugares();
      $scope.consultar_stats();
      $scope.consultar_historial();
    });
    $scope.alertInsertar=true;
  };

  $scope.tiposincidentes = [];
  $http.get("./model_tiposincidentes_qry.php").then(function (response) {
    $scope.incidentes_menu = response.data;
    console.log($scope.incidentes_menu);

    $scope.incidente_seleccionado;
  });
});
