DROP TABLE IF EXISTS Lugares;
CREATE TABLE Lugares (
    idLugar int AUTO_INCREMENT PRIMARY KEY,
    Nombre varchar(200)
);

DROP TABLE IF EXISTS TiposIncidentes;
CREATE TABLE TiposIncidentes (
    idIncidente int AUTO_INCREMENT PRIMARY KEY,
    nombre varchar(200)
);

DROP TABLE IF EXISTS Seguridad;
CREATE TABLE Seguridad (
    idSeguridad int AUTO_INCREMENT PRIMARY KEY,
    fecha TIMESTAMP DEFAULT NOW(),
    idLugar int,
    idIncidente int,
    FOREIGN KEY (idLugar) REFERENCES Lugares(idLugar) ON DELETE CASCADE,
    FOREIGN KEY (idIncidente) REFERENCES TiposIncidentes(idIncidente) ON DELETE CASCADE
);

INSERT INTO TiposIncidentes (nombre) VALUES ('Falla eléctrica');
INSERT INTO TiposIncidentes (nombre) VALUES ('Fuga de herbívoro');
INSERT INTO TiposIncidentes (nombre) VALUES ('Fuga de velociraptors');
INSERT INTO TiposIncidentes (nombre) VALUES ('Fuga de trex');
INSERT INTO TiposIncidentes (nombre) VALUES ('Robo de ADN');
INSERT INTO TiposIncidentes (nombre) VALUES ('Auto descompuesto');
INSERT INTO TiposIncidentes (nombre) VALUES ('Visitantes en zona no autorizada');

INSERT INTO Lugares (Nombre) VALUES ('Centro turístico');
INSERT INTO Lugares (Nombre) VALUES ('Laboratorios');
INSERT INTO Lugares (Nombre) VALUES ('Restaurante');
INSERT INTO Lugares (Nombre) VALUES ('Centro operativo');
INSERT INTO Lugares (Nombre) VALUES ('Triceratops');
INSERT INTO Lugares (Nombre) VALUES ('Dilofosaurios');
INSERT INTO Lugares (Nombre) VALUES ('Velociraptors');
INSERT INTO Lugares (Nombre) VALUES ('Trex');
INSERT INTO Lugares (Nombre) VALUES ('Planicie de los herbívoros');
