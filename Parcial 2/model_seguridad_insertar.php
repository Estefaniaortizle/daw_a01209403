<?php
include './util.php';
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
$con = connect_db();
if (!isset($_POST)) {
    die();
}
$postdata = file_get_contents("php://input");
$data = json_decode($postdata, true);
$idLugar = $data[0];
$idIncidente = $data[1];
insertar_seguridad($idLugar, $idIncidente, $con);
