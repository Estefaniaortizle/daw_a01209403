<?php
include './util.php';
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
$con = connect_db();
if (!isset($_POST)) {
    die();
}
$postdata = file_get_contents("php://input");
$data = json_decode($postdata, true);
$nombre = $data[0]['nombre'];
insertar_lugar($nombre, $con);
mysqli_close($con);
?>
